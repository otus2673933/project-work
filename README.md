## Создание кластера
1. Получаем токен Yandex Cloud по ссылке https://cloud.yandex.ru/docs/iam/operations/iam-token/create и заполняем в переменную YC_TOKEN в гитлабе
2. Создаём токен для доступа в гитлаб по ссылке https://gitlab.com/-/profile/personal_access_tokens и заполняем по в переменную GITLAB_TOKEN. Заполняем GITLAB_USER, GITLAB_BRANCH
3. Заполняем переменные для создания кластера
  - CLUSTERNAME - Имя кластера и имя группы нод
  - COUNT_NODE - количество нод
  - CORES - количество ядер
  - DISK_SIZE - размер дискового пространства
  - MEMORY - объём оперативной памяти
  - SERVICE_ACCOUNT_NAME - имя сервисного аккаунта для создания кластера
4. Заполняем переменные для FLUX
  - FLUXCD_PATH - путь с конфигами для FLUX
5. Нажимаем CreateK8S

## Компоненты кластера
Компоненты кластера находятся тут clusters/otus/kustomization
1. kube-prometheus-stack
2. cert-manager
3. bank-vaults
4. harbor
5. loki
6. promtail
7. Приложение

## kube-prometheus-stack
Доступ по адресу https://grafana.${IP}.nip.io

## bank-vaults
Доступ по адресу https://vault.${IP}.nip.io

## harbor
Доступ по адресу https://harbor.${IP}.nip.io

## Приложение
Доступ по адресу https://frontend.${IP}.nip.io

Где ${IP} - ip адрес балансера